# Sumário
[[_TOC_]]

# Requisitos do Projeto

## API para gerir uma WishList

Para implementar uma API que atenda aos requisitos do projeto, foi necessário definir seus *endpoints* e recursos envolvidos.

Ao analisar o seguinte requisito:

    O usuário deve poder incluir um produto em sua lista de desejos

A lista de desejos foi abstraída como uma coleção de itens, cada um representando um produto. Um produto foi determinado como um recurso da API. A criação de recursos por meio de uma API é feita normalmente a partir do método POST de um *endpoint*, definida como:

    POST /products

Porém, o método POST deve conter em seu corpo informações para a construção desse recurso. Essas informações devem corresponder às seguintes exigências:

- Título do Produto (obrigatório)
- Descrição (opcional)
- Link de onde encontrar (opcional)
- Foto (opcional)

A foto, por ser um arquivo, exige que as informações do método POST sejam enviadas por meio de um formulário do tipo *multi-part* junto com os demais dados. 

Ao mandar uma *request* para essa *endpoint*, um validador testa se o usuário enviou corretamente o formulário, verificando, por exemplo, se o campo referente ao título do produto foi fornecido.

O passo seguinte foi determinar como as informações oriundas do método POST devem ser usadas para criar o recurso. Para facilitar a interoperabilidade entre o ```web framework``` (Falcon) o banco de dados (PostgreSQL) foi utilizada a ferramenta [`SQLAlchemy`](https://www.sqlalchemy.org/), um mapeador relacional de objeto (**ORM**) que permite a operação de um banco de dados em um nível maior de abstração.

O título, descrição e link do produto podem ser modelados como atributos do tipo *string* e armazenados no banco de dados. Porém o mesmo não é recomendado para arquivos relativamente grandes como imagens, devido ao alto custo de armazenamento e processamento. A alternativa adotada foi a de guardá-las no próprio sistema de arquivos e sua localização mapeada como uma uri (tipo *string*) com as demais informações do produto.

Para atender o próximo requisito:

    O usuário deve poder informar se já ganhou/comprou um item

Um novo atributo foi determinado para o modelo ```Product``` denominado ```acquired```. Seu estado é de natureza binária - adquiriu ou não adquiriu -, portanto, seu tipo é `boolean`. 

Existem duas alternativas que foram implementadas para permitir a inserção desse tipo de informação. A primeira é utilizando o método PUT:

    PUT /products/{public_id}

Da mesma forma que no método POST, PUT exige o envio de um formulário *multi-part* (imagens poder ser enviadas), porém com todas as informações que compõem o estado do recurso (no método POST somente o título é obrigatório). A outra alternativa é o método PATCH:

    PATCH /products/{public_id}

Este método permite a modificação de apenas alguns estados do recurso. O método PATCH poder ser mais conveniente no caso de o usuário querer informar somente se adquiriu ou não o produto.

O *path parameter* ```public_id``` é necessário para poder acessar cada produto individualmente, sendo ela internamente gerada no backend ao se criar um produto pelo método POST.

O requisito

    Deve existir um endpoint para trazer um item da lista de forma randômica

foi implementado definindo uma *endpoint* distinta :

    GET /products/random

a decisão de se criar uma nova *endpoint* para essa funcionalidade foi tomada para permitir que o método GET em ```/products``` retorne a lista de produtos adicionados pelo usuário. Como ```/products/random``` não representa realmente a localização de um recurso específico, a **uri** do produto randomicamente selecionado é retornado na resposta da requisição.

## Outros métodos

Os métodos anteriormente descritos permitem criar e atualizar produtos, mas para poder gerir uma lista de desejos, outras operações são necessárias como, por exemplo, o acesso do produto para consulta ou qualquer outra finalidade. Isso é viabilizado por meio da operação e *endpoint*:

    GET /products

que retornará uma lista de produtos. O acesso individual de um produto é feito em:

    GET /products/{public_id}

Por fim, outra operação importante é a remoção de um recurso:

    DEL /products/{public_id}

Operações do tipo GET e DEL não exigem informações extras (a exceção do *path parameter* ```public_id```) para serem efetuadas.

# Suporte a múltiplos usuários

O suporte para múltiplos usuários adiciona uma nova camada de complexidade a API que exige a definição de um novo recurso para representá-los, além da definição de novos *endpoints*. O tipo de autenticação utilizado foi o baseado em tokens ([`JSON Web Tokens`](https://jwt.io/)) por ser um método que preserva a característica *stateless* de uma arquitetura do tipo REST.

Um usuário pode ser criado por meio de um:

    POST /users

fornecendo no corpo um objeto **json** com um ```username```, ```password``` e ```confirm_password```, o *responder* correspondente a essa requisição verifica se o nome do usuário é único e se as duas senhas são idênticas. Se os dados forem válidos, então um novo usuário será criado contendo seu nome e a *hash* da senha fornecida, e em seguida, armazenados no banco de dados. Um *public_id* no formato *uui4* também é gerado durante esse processo para compor a **uri** do recurso.

O 'log in' de um usuário é realizado ao fornecer seu nome e senha no formato **json** em:

    POST /login

Em ambos os processos (criação de usuários e 'log in'), um token que servirá como credencial do usuário é retornado:

    {
        "token": eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoidGVzdCJ9LCJpYXQiOjE2MTM0OTExNjUsIm5iZiI6MTYxMzQ5MTE2NSwiZXhwIjoxNjEzNTc3NTY1fQ.HOraDpFjfyrrfIo4nVb3JWV6TObwa_zHUr1M-ZVMpHM 
        ...
    }

Essa chave deve ser guardada para acessar as *endpoints* dos recursos que exigem autenticação. A autenticação nesses casos deve começar com a inserção do **token** no *headers* das respectivas requisições:

    Authentication: jwt {token}

Se o **token** for validado, o sistema permitirá o acesso aos recursos requisitados.


# Documentação da API

A documentação completa da API pode ser obtida em:

- https://app.swaggerhub.com/apis/adndgkorg/WishListAPI/1.0#/

# Arquitetura do Projeto

## Visão Geral

O sistema implementado opera a partir de imagens construídas e orquestradas pela ferramenta [`Docker Compose`](https://docs.docker.com/compose/install/).

Docker Compose inicializa os seguintes serviços que serão discutidos mais adiante:

- **nginx** - servidor proxy
- **api** - servidor web para processar requisições
- **postgres** - banco de dados
- **client** - frontend que consumirá a API

O imagem abaixo ilustra as dependências entre os serviços:

```mermaid
graph TB

  Node1 --> Node3
  subgraph "Backend"
  Node3[api] --> postgres
  end

  Node1 --> Node2
  subgraph "Frontend"
  Node2[client]
  end

  subgraph "Proxy Server" 
  Node1[nginx]
end
```
## Serviços

### nginx

O servidor Nginx permite o roteamento das requisições via web a partir de mapeamentos definidos em seu arquivo de configuração.

Essa capacidade, aliada aos baixos custo tanto de processamento quanto de memória serviram de critério para sua adoção no projeto.

Todas as requisições passam por esse servidor que está configurado para ouvir a porta **8000** (mapeado para a porta **80** dentro do container).

Considerando o seguinte prefixo:

    localhost:8000

A **api** será acessada a partir de:

    localhost:8000/api

Por sua vez, o **frontend** será acessado pelo endereço raiz:

    localhost:8000/

### api e postgres

Ambos os serviços compõem o backend do aplicativo, **postgres** refere-se ao banco de dados PostgreSQL utilizado para armazenar informações relevantes à API.

O serviço **api** faz uso direto do **postgres**, dependendo diretamente deste para o correto processamento das requisições. 

Os *endpoints* da API foram implementados utilizando o framework [`Falcon`](https://falcon.readthedocs.io/en/stable/index.html).

### client

Um frontend foi implementado com o framework [`React`]([https://reactjs.org/]). A interface gráfica permite as seguintes funcionalidades:

- cadastramento de novos usuários
- *login* de usuários
- visualização da lista de desejos
- adição de um produto
- atualização de um produto
- remoção de um produto

Cada usuário tem acesso a sua própria de lista de desejos e total liberdade para gerí-la. 
