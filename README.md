# Wishlist App

## Instalação
Dependência:
- [`Docker Compose`](https://docs.docker.com/compose/install/)

Execute o seguinte comando no diretório do projeto:

    docker-compose build

## Uso
Entre com o seguinte comando para rodar as imagens construídas:

    docker-compose up

Os serviços da API e do frontend estarão acessíveis após a execução do comando acima.

### API

Este app atualmente roda somente em uma máquina local. 

Depois de gerar as imagens e iniciar os serviços, acesse a API em:

    localhost:8000/api/

Para mais informações acesse a documentação da API [`aqui`](https://app.swaggerhub.com/apis/adndgkorg/WishListAPI/1.0#/).

**Nota**: os *endpoints* restritos aos usuários cadastrados devem ser acessados com o fornecimento uma chave (token) no campo **Authorization** do *headers* de uma requisição: 

    Authorization: jwt {token}

Essa chave é enviada ao usuário no corpo da resposta de uma requisição do tipo criar um novo usuário, ou ao se fazer o *login*.

Um usuário do tipo **admin** é gerado na primeira vez que o serviço api é inicializado (**username**: admin, **password**: password). Este possui todos os privilégios de um usuário comum (acesso aos seus próprios item da lista de desejos), além de poder acessar, atualizar ou remover outros usuários.

### Frontend

Acesse o cliente frontend em:

    localhost:8000/

No aplicativo você pode criar uma conta nova ou entrar com a conta do admin para gerir a lista de desejos.

### Database Command

A banco de dados pode voltar para sua configuração inicial seguindo os seguintes passos:
- Encontre o nome do serviço da api:

    ```docker ps```

- Acesse-o entrando com o comando:

    ```docker exec -it {nome_do_serviço_da_api} /bin/bash```

- No diretório ```/app```, execute o seguinte comando:

    ```python manager.py recreatetables```

Este comando irá remover e criar novamente todas as tabelas do banco de dados, além de gerar um novo usuário do tipo admin.

### Testes

Alguns testes estão disponíveis para serem executados. Entre com o seguinte comando para acessar o `shell` do serviço da api:

    docker exec -it {nome_do_serviço_da_api} /bin/bash

Execute os testes com o commando ```pytest```:

    pytest tests

Para testar com maior interação os *endpoints*, recomenda-se o uso do app [`Postman`](https://www.postman.com/). Uma coleçao para teste pode ser obtida [`aqui`](https://gitlab.com/adndgk1/wishlist/-/blob/master/backend/tests/WishList.postman_collection.json) e ser importada no Postman.

**Nota:** é necessário baixar o **Postman Agent** para poder fazer testes com upload de arquivos.
