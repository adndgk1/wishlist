export const AUTH_USER = "auth_user";
export const AUTH_ERROR = "auth_error";

export const SELECT_PRODUCT = "select_product";
export const CREATE_PRODUCT = "create_product";
export const UPDATE_PRODUCT = "update_product";
export const REMOVE_PRODUCT = "remove_product";
export const PATCH_PRODUCT = "patch_product";
export const GET_PRODUCT_LIST = "get_product_list";
