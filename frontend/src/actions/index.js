import axios from "axios";
import {
  AUTH_USER,
  AUTH_ERROR,
  CREATE_PRODUCT,
  UPDATE_PRODUCT,
  REMOVE_PRODUCT,
  GET_PRODUCT_LIST,
  PATCH_PRODUCT,
  SELECT_PRODUCT,
} from "./types";

export const signup = (formProps, callback) => async (dispatch) => {
  try {
    const response = await axios.post(
      "http://localhost:8000/api/users",
      formProps
    );
    dispatch({ type: AUTH_USER, payload: response.data.token });
    localStorage.setItem("token", response.data.token);
    callback();
  } catch (e) {
    alert("Username already exists and/or passwords do not match.");
  }
};

export const signout = () => {
  localStorage.removeItem("token");
  return { type: AUTH_USER, payload: "" };
};

export const signin = (formProps, callback) => async (dispatch) => {
  try {
    const response = await axios.post(
      "http://localhost:8000/api/login",
      formProps
    );
    dispatch({ type: AUTH_USER, payload: response.data.token });
    localStorage.setItem("token", response.data.token);
    callback();
  } catch (e) {
    alert("Invalid username and/or password.");
  }
};

export const create_product = (formProps, token) => async (dispatch) => {
  try {
    const data = new FormData();

    Object.keys(formProps).map(function (key, index) {
      if (key === "image") {
        data.append(key, formProps[key][0]);
      } else {
        data.append(key, formProps[key]);
      }
    });

    let config = {
      headers: {
        "Content-type": "multipart/form-data",
        Authorization: "jwt " + token,
      },
    };
    const response = await axios.post(
      "http://localhost:8000/api/products",
      data,
      config
    );
    dispatch({ type: CREATE_PRODUCT, payload: response.data });
  } catch (e) {
    alert("Could not add a product (title required).");
  }
};

export const get_product_list = (token) => async (dispatch) => {
  try {
    let config = {
      headers: {
        Authorization: "jwt " + token,
      },
    };

    const response = await axios.get(
      "http://localhost:8000/api/products",
      config
    );
    dispatch({ type: GET_PRODUCT_LIST, payload: response.data });
  } catch (e) {}
};

export const patch_product = (formProps, token, uri) => async (
  dispatch
) => {
  const data = new FormData();
  
  // set provided values to be sent to the server
  Object.keys(formProps).map(function (key, index) {
    if (key === "image") {
      data.append(key, formProps[key][0]);
    } else {
      data.append(key, formProps[key]);
    }
  });
  
  let config = {
    headers: {
      "Content-type": "multipart/form-data",
      Authorization: "jwt " + token,
    },
  };
  const response = await axios.patch(uri, data, config);
  dispatch({ type: PATCH_PRODUCT, payload: response.data });
};

export const remove_product = (token, uri) => async (dispatch) => {
  let config = {
    headers: {
      "Content-type": "multipart/form-data",
      Authorization: "jwt " + token,
    },
  };
  const response = await axios.delete(uri, config);
  dispatch({ type: REMOVE_PRODUCT, payload: response.data });
};

export const update_product = (formProps, token, uri) => async (dispatch) => {
  const data = new FormData();
  const required_fields = ["title", "description", "link", "image", "acquired"];
  
  // assure that update request contains all required fields
  required_fields.forEach((field) => {
    if (!(field in formProps)) {
      formProps[field] = "";
    }
  });

  // set all values to be sent to the server
  Object.keys(formProps).map(function (key, index) {
    if (key === "image") {
      if (typeof formProps[key][0] !== "undefined") {
        data.append(key, formProps[key][0]);
      } else {
        data.append(key, new File([""], ""));
      }
    } else {
      data.append(key, formProps[key]);
    }
  });

  let config = {
    headers: {
      "Content-type": "multipart/form-data",
      Authorization: "jwt " + token,
    },
  };
  const response = await axios.put(uri, data, config);
  dispatch({ type: UPDATE_PRODUCT, payload: response.data });
  return response;
};

export const select_product = (product) => (dispatch) =>
  dispatch({ type: SELECT_PRODUCT, payload: product });
