import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./HeaderStyle.scss";

class Header extends Component {
  renderLinks() {
    if (this.props.authenticated) {
      return (
        <div className="header-div">
          <div>
            <Link className="link" to="/">
              Home
            </Link>
            <Link className="link" to="/wishlist">
              WishList
            </Link>
          </div>
          <div>
            <Link className="link" to="/signout">
              Sign out
            </Link>
          </div>
        </div>
      );
    } else {
      return (
        <div className="header-div">
          <div>
            <Link className="link" to="/">
              Home
            </Link>
          </div>
          <div>
            <Link className="link" to="/signin">
              Log in
            </Link>
            or
            <Link className="link" to="/signup">
              Sign up
            </Link>
          </div>
        </div>
      );
    }
  }

  render() {
    return <div className="header">{this.renderLinks()}</div>;
  }
}

function mapStateToPros(state) {
  return { authenticated: state.auth.authenticated };
}

export default connect(mapStateToPros)(Header);
