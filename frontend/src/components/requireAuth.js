import React, { Component } from "react";
import { connect } from "react-redux";

export default (WrapedComponent) => {
  class AuthRestrictedComponent extends Component {
    componentDidMount() {
      this.gotToHomePage();
    }

    componentDidUpdate() {
      this.gotToHomePage();
    }

    gotToHomePage() {
      if (!this.props.auth) {
        this.props.history.push("/");
      }
    }
    render() {
      return <WrapedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { auth: state.auth.authenticated };
  }

  return connect(mapStateToProps)(AuthRestrictedComponent);
};
