import React from "react";

import "./HomePageStyle.scss";

export default () => {
  return (
    <div className="homepage-container">
      <h3>Welcome to WishList app!</h3>
      <ul>
        <li>Create an account or log in to access the app</li>
        <li>Click on the 'WishList' button that will appear once logged in</li>
      </ul>
    </div>
  );
};
