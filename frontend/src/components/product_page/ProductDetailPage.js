import React, { Component } from "react";
import requireAuth from "../requireAuth";
import { connect } from "react-redux";
import { compose } from "redux";
import axios from "axios";
import * as actions from "../../actions";
import ProductPutForm from "./ProductPutForm";

import "./ProductDetailPageStyle.scss";

class ProductDetailPage extends Component {
  constructor() {
    super();

    this.state = {
      product: {},
    };
  }

  updateHandler = (updated_product) => {
    this.setState({ product: updated_product });
  };

  preLoadHandler = async (callback) => {
    let config = {
      headers: {
        Authorization: "jwt " + this.props.token,
      },
    };
    const product_id = this.props.match.params.id;
    const product_uri = "http://localhost:8000/api/products" + "/" + product_id;
    const response = await axios.get(product_uri, config);
    this.setState({ product: response.data });
    callback();
  };

  render() {
    return (
      <div className="product-page">
        <div className="product-detail-div">
          <div className="product-detail-image-div">
            <img
              className="product-detail-image"
              src={
                this.state.product.image_uri
                  ? this.state.product.image_uri
                  : process.env.PUBLIC_URL + "/no_image.png"
              }
              alt={this.state.product.title}
            />
          </div>
          <div className="product-detail-info-div">
            <div className="detail-table">
              <div className="detail-row">
                <div className="detail-label">Title:</div>
                <div className="detail-value">{this.state.product.title}</div>
              </div>
              <div className="detail-row">
                <div className="detail-label">Description:</div>
                <div className="detail-value">
                  {this.state.product.description}
                </div>
              </div>
              <div className="detail-row">
                <div className="detail-label">Link:</div>
                <div className="detail-value">{this.state.product.link}</div>
              </div>
              <div className="detail-row">
                <div className="detail-label">Acquired:</div>
                <div className="detail-value">
                  {this.state.product.acquired ? "yes" : "no"}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="detail-form-div">
          <ProductPutForm
            updateHandler={this.updateHandler}
            preLoadHandler={this.preLoadHandler}
            product={this.state.product}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    token: state.auth.authenticated,
    products: state.wishlist.products,
  };
}

export default compose(
  connect(mapStateToProps, actions),
  requireAuth
)(ProductDetailPage);
