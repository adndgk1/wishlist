import React, { Component } from "react";
import { reduxForm, Field, change } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions";
import UploadFile from "../UploadFile";
import axios from "axios";

import "./ProductPutFormStyle.scss";

class ProductPutForm extends Component {
  constructor() {
    super();
    this.state = {
      previewImage: null,
    };
  }

  onSubmit = async (formProps) => {
    const response = await this.props.update_product(
      formProps,
      this.props.token,
      this.props.product.uri
    );
    this.props.updateHandler(response.data);
  };

  componentDidMount() {
    // allow pre-loading form field with initial product values.
    this.props.preLoadHandler(this.formUpdateField);
  }

  formUpdateField = () => {
    this.props.change("title", this.props.product.title);
    this.props.change("description", this.props.product.description);
    this.props.change("link", this.props.product.link);
    this.props.change("acquired", this.props.product.acquired);
  };

  handleChange = (event) => {
    let previewImage = "";
    if (event.target.files.length === 1) {
      previewImage = URL.createObjectURL(event.target.files[0]);
    }
    this.setState({ previewImage });
  };

  render() {
    return (
      <div className="put-form-div">
        <form
          className="put-form"
          onSubmit={this.props.handleSubmit(this.onSubmit)}
        >
          <fieldset>
            <label>Title: </label>
            <Field
              name="title"
              type="text"
              component="input"
              autoComplete="off"
            />
          </fieldset>
          <fieldset>
            <label>Description: </label>
            <Field
              name="description"
              type="text"
              component="input"
              autoComplete="off"
            />
          </fieldset>
          <fieldset>
            <label>Link: </label>
            <Field
              name="link"
              type="text"
              component="input"
              autoComplete="off"
            />
          </fieldset>
          <fieldset>
            <label>Acquired: </label>
            <Field
              name="acquired"
              type="checkbox"
              component="input"
              autoComplete="off"
            />
          </fieldset>
          <fieldset>
            <label>Image: </label>
            <Field
              className="image-field"
              name="image"
              type="file"
              accept=".png, .jpg, jpeg"
              onChange={this.handleChange}
              component={UploadFile}
            />
          </fieldset>
          <div className="submit-area">
            <button>Update</button>
          </div>
        </form>
        <div className="preview-image-div">
          <div className="preview-image-title">
            <div>Image Preview</div>
          </div>
          <div className="preview-image-frame">
            <img
              src={
                this.state.previewImage
                  ? this.state.previewImage
                  : process.env.PUBLIC_URL + "/no_image.png"
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    token: state.auth.authenticated,
  };
}

export default compose(
  connect(mapStateToProps, { change, ...actions }),
  reduxForm({ form: "product-put-form" })
)(ProductPutForm);
