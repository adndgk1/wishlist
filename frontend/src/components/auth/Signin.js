import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions";

import "./SigninStyle.scss";

class Signin extends Component {
  onSubmit = (formProps) => {
    this.props.signin(formProps, () => {
      this.props.history.push("/wishlist");
    });
  };

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="signin-form-div">
        <div className="signin-form-header">
          <h3>Log in</h3>
        </div>
        <form className="signin-form" onSubmit={handleSubmit(this.onSubmit)}>
          <fieldset>
            <label>Username: </label>
            <Field
              name="username"
              type="text"
              component="input"
              autoComplete="none"
            />
          </fieldset>
          <fieldset>
            <label>Password: </label>
            <Field
              name="password"
              type="password"
              component="input"
              autoComplete="none"
            />
          </fieldset>
          <div className="submit-area">
            <button className="submit-button">Log In</button>
          </div>
        </form>
      </div>
    );
  }
}

export default compose(
  connect(null, actions),
  reduxForm({ form: "signin" })
)(Signin);
