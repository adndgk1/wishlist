import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions";

import "./SignupStyle.scss";

class Signup extends Component {
  onSubmit = (formProps) => {
    this.props.signup(formProps, () => {
      this.props.history.push("/wishlist");
    });
  };

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="signup-form-div">
        <div className="signup-form-header">
          <h3>Sign up</h3>
        </div>
        <form className="signup-form" onSubmit={handleSubmit(this.onSubmit)}>
          <fieldset>
            <label>Username: </label>
            <Field
              name="username"
              type="text"
              component="input"
              autoComplete="none"
            />
          </fieldset>
          <fieldset>
            <label>Password: </label>
            <Field
              name="password"
              type="password"
              component="input"
              autoComplete="none"
            />
          </fieldset>
          <fieldset>
            <label>Corfirm password: </label>
            <Field
              name="confirm_password"
              type="password"
              component="input"
              autoComplete="none"
            />
          </fieldset>
          <div className="submit-area">
            <button className="submit-button">Sign Up</button>
          </div>
        </form>
      </div>
    );
  }
}

export default compose(
  connect(null, actions),
  reduxForm({ form: "signup" })
)(Signup);
