import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions";
import UploadFile from "../UploadFile";
import axios from "axios";

import "./ProductCreateFormStyle.scss";


class ProductCreateForm extends Component {
  onSubmit = (formProps) => {
    console.log(formProps);

    // form creates 'image' key even if an image was not selected.
    // remove 'image' for appropriate data formatting to endpoint.
    if ("image" in formProps && formProps["image"].length === 0) {
      delete formProps["image"];
    }
    this.props.create_product(formProps, this.props.token);
  };

  render() {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit)}>
        <fieldset>
          <label>Title (required): </label>
          <Field
            name="title"
            type="text"
            component="input"
            autoComplete="off"
          />
        </fieldset>
        <fieldset>
          <label>Description: </label>
          <Field
            name="description"
            type="text"
            component="input"
            autoComplete="off"
          />
        </fieldset>
        <fieldset>
          <label>Link: </label>
          <Field name="link" type="text" component="input" autoComplete="off" />
        </fieldset>
        <fieldset>
          <label>Image: </label>
          <Field
            className="image-field"
            name="image"
            type="file"
            accept=".png, .jpg, jpeg"
            component={UploadFile}
          />
        </fieldset>
        <div className="submit-area">
          <button>Add</button>
        </div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return { token: state.auth.authenticated };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "product-form" })
)(ProductCreateForm);
