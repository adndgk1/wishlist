import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import "./ProductItemStyle.scss";
import * as actions from "../../actions";

class ProductItem extends Component {
  constructor() {
    super();

    this.state = {
      checkbox: false,
    };
  }

  componentDidMount() {
    this.setState({ checkbox: this.props.data.acquired });
  }

  onCheckboxChange = (e) => {
    this.props.patch_product(
      { acquired: !this.props.data.acquired },
      this.props.token,
      this.props.data.uri
    );
  };

  onClickRemove = (e) => {
    this.props.remove_product(this.props.token, this.props.data.uri);
  };

  render() {
    const acquired_true = this.props.data.acquired ? "acquired-true" : "";
    return (
      <div className={"product-item " + acquired_true}>
        <Link
          className="product-detail-link"
          to={`/products/${this.props.data.public_id}`}
        >
          <div className="product-image-div">
            <img
              className="product-image"
              src={
                this.props.data.image_uri
                  ? this.props.data.image_uri
                  : process.env.PUBLIC_URL + "/no_image.png"
              }
              alt={this.props.data.title}
            ></img>
          </div>
          <div className="product-title-div">
            <div className="product-title">{this.props.data.title}</div>
          </div>
        </Link>
        <div className="product-acquired">
          <div
            className="product-acquired-checkbox"
            onClick={this.onCheckboxChange}
          >
            <input
              type="checkbox"
              name="acquired"
              onChange={this.onCheckboxChange}
              checked={this.props.data.acquired}
            />
            <label>Acquired</label>
          </div>
        </div>
        <div className="product-remove-div" onClick={this.onClickRemove}>
          <img
            className="product-remove"
            src={process.env.PUBLIC_URL + "/remove_icon.png"}
            alt={this.props.data.title}
          ></img>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.authenticated,
  };
};

export default connect(mapStateToProps, actions)(ProductItem);
