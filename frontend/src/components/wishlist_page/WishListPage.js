import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import axios from "axios";
import * as actions from "../../actions";

import requireAuth from "../requireAuth";
import ProductItem from "./ProductItem";
import RandomSelector from "./RandomSelector";
import ProductCreateForm from "./ProductCreateForm";

import "./WishListPageStyle.scss";

class WishList extends Component {
  componentDidMount() {
    this.props.get_product_list(this.props.token);
  }

  render() {
    return (
      <div className="wishlist-page">
        <div className="left-container">
          <div className="form-area">
            <div className="form-title">
              <h3>Add a product</h3>
            </div>
            <ProductCreateForm />
          </div>
          <div className="random-selector-area">
            <RandomSelector {...this.props} />
          </div>
        </div>
        <div className="wishlist">
          {this.props.products
            .slice()
            .reverse()
            .map((product) => {
              return <ProductItem key={product.public_id} data={product} />;
            })}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    token: state.auth.authenticated,
    products: state.wishlist.products,
  };
}

export default compose(
  connect(mapStateToProps, actions),
  requireAuth
)(WishList);
