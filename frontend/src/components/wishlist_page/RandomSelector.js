import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

import * as actions from "../../actions";

import "./RandomSelectorStyle.scss";

class RandomSelector extends Component {
  onClickRandom = async () => {
    let config = {
      headers: {
        Authorization: "jwt " + this.props.token,
      },
    };

    const product_uri = "http://localhost:8000/api/products/random";
    const response = await axios.get(product_uri, config);
    this.props.history.push(`/products/${response.data.public_id}`);
  };

  render() {
    return (
      <div className="random-container">
        <div className="random-header">
          <h3>Random Selector</h3>
        </div>
        <div className="random-button-container">
          <button onClick={this.onClickRandom}>Go!</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.authenticated,
  };
};

export default connect(mapStateToProps, actions)(RandomSelector);
