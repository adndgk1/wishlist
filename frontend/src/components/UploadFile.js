// https://stackoverflow.com/questions/43996895/react-redux-upload-file-errors
const UploadFile = ({
  input: { value: omitValue, ...inputProps },
  meta: omitMeta,
  ...props
}) => <input type="file" {...inputProps} {...props} />;

export default UploadFile;