import React from "react";
import Header from "./Header";

import "./AppStyle.scss";

export default ({ children }) => {
  return (
    <div className="page-container">
      <Header />
      <div className="content">{children}</div>
    </div>
  );
};
