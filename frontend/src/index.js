import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";

import reducers from "./reducers";
import App from "./components/App";
import HomePage from "./components/home_page/HomePage";
import Signup from "./components/auth/Signup";
import WishListPage from "./components/wishlist_page/WishListPage";
import Signout from "./components/auth/Signout";
import Signin from "./components/auth/Signin";
import ProductDetailPage from "./components/product_page/ProductDetailPage";

const store = createStore(
  reducers,
  {
    auth: { authenticated: localStorage.getItem("token") },
  },
  compose(
      
    applyMiddleware(reduxThunk),
    window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : f => f
  )
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App>
        <Route path="/" exact component={HomePage} />
        <Route path="/signup" component={Signup} />
        <Route path="/wishlist" component={WishListPage} />
        <Route path="/signout" component={Signout} />
        <Route path="/signin" component={Signin} />
        <Route path="/products/:id" exact component={ProductDetailPage} />
      </App>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
