import {
  CREATE_PRODUCT,
  UPDATE_PRODUCT,
  REMOVE_PRODUCT,
  GET_PRODUCT_LIST,
  PATCH_PRODUCT,
  SELECT_PRODUCT,
} from "../actions/types";

const INITIAL_STATE = {
  products: [],
  selected_product: {},
};

export default function (state = INITIAL_STATE, action) {
  let products;

  switch (action.type) {
    case SELECT_PRODUCT:
      return {
        ...state,
        selected_product: action.payload,
      };
    case CREATE_PRODUCT:
      return {
        products: [...state.products, action.payload],
      };
    case PATCH_PRODUCT:
      products = state.products.map((item) => {
        if (item.public_id === action.payload.public_id) {
          return action.payload;
        }
        return item;
      });
      return { products };
    case UPDATE_PRODUCT:
      products = state.products.map((item) => {
        if (item.public_id === action.payload.public_id) {
          return action.payload;
        }
        return item;
      });
      return {
        products,
      };
    case REMOVE_PRODUCT:
      products = state.products.filter((item) => {
        return item.public_id !== action.payload.public_id;
      });
      console.log(products.length);
      return { products };
    case GET_PRODUCT_LIST:
      return {
        products: action.payload,
      };
    default:
      return state;
  }
}
