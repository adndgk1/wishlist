import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import auth from "./auth";
import wishlist from "./wishlist";

export default combineReducers({
  auth,
  wishlist,
  form: formReducer,
});
