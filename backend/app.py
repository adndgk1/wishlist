import os
import falcon
import falcon_sqla
from falcon_auth import FalconAuthMiddleware, JWTAuthBackend
from falcon_multipart.middleware import MultipartMiddleware

from envconf import config
from database.manager import DBManager
from database.models import User
from resources.product import ProductsResource
from resources.image import ImagesResource
from resources.user import UsersResource
from resources.auth_session import AuthSessionResource
from image_storage.image_store import ImageStore


def create_api(image_store):
    user_loader = lambda username, password: { 'username': username }

    # SQLAlchemy session middleware configuration
    db_manager = DBManager()
    manager = falcon_sqla.Manager(db_manager.get_sqla_engine())
    session_middleware = manager.middleware

    # authentication middleware configuration
    user_loader = User.get_user_loader(db_manager.session)
    jwt_key = config['jwt_key']
    auth_backend = JWTAuthBackend(user_loader, jwt_key)
    auth_middleware = FalconAuthMiddleware(auth_backend,
                    exempt_routes=['/login'], exempt_methods=['HEAD'])

    multipart_middleware = MultipartMiddleware()

    api = falcon.API(middleware=[
        auth_middleware,
        session_middleware,
        multipart_middleware,
        ])

    images = ImagesResource(auth_backend, image_store)
    products = ProductsResource(image_store)
    users = UsersResource(auth_backend)
    auth_session = AuthSessionResource(auth_backend)

    api.add_route('/login', auth_session)
    api.add_route('/images', images)
    api.add_route('/images/{name}', images)
    api.add_route('/users', users, suffix='collection')
    api.add_route('/users/{public_id}', users)
    api.add_route('/products', products, suffix='collection')
    api.add_route('/products/{public_id}', products)
    api.add_route('/products/random', products, suffix='random')

    return api


def get_api():
    storage_path = os.environ.get('STORAGE_PATH', './image_storage/uploads')
    image_store = ImageStore(storage_path)
    return create_api(image_store)

api = get_api()