import io
import os
import glob
import re
import uuid
import mimetypes

import falcon
import msgpack


class ImageStore(object):

    _CHUNK_SIZE_BYTES = 4096
    _IMAGE_NAME_PATTERN = re.compile(
        '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\\.[a-z]{2,4}$'
    )

    def __init__(self, storage_path, uuidgen=uuid.uuid4, fopen=io.open):
        self._storage_path = storage_path
        self._uuidgen = uuidgen
        self._fopen = fopen
        if not os.path.exists(self._storage_path):
            os.makedirs(self._storage_path)

    def _save(self, image_stream, image_content_type):
        ext = mimetypes.guess_extension(image_content_type)
        name = '{uuid}{ext}'.format(uuid=self._uuidgen(), ext=ext)
        image_path = os.path.join(self._storage_path, name)

        with self._fopen(image_path, 'wb') as image_file:
            while True:
                chunk = image_stream.read(self._CHUNK_SIZE_BYTES)
                if not chunk:
                    break

                image_file.write(chunk)
        return name

    def save(self, image):
        image_uri = ''
        if image != b'':
            image_name = self._save(image.file, image.type)
            image_uri = '/images/' + image_name
        return image_uri

    def open(self, name):
        if not self._IMAGE_NAME_PATTERN.match(name):
            raise IOError('File not found')

        image_path = os.path.join(self._storage_path, name)
        stream = self._fopen(image_path, 'rb')
        content_length = os.path.getsize(image_path)

        return stream, content_length

    def delete(self, image_uri):
        if image_uri == '':
            return
        delete_image_name = image_uri.split("/")[-1]
        for image in glob.glob(self._storage_path + '/*'):
            image_name = image.split("/")[-1]
            if image_name == delete_image_name:
                os.remove(image)

    def remove_all_images(self):
        files = glob.glob(self._storage_path + '/*')
        for f in files:
            os.remove(f)
