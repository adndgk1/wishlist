import falcon
import json
from werkzeug.security import generate_password_hash, check_password_hash
from falcon_auth import JWTAuthBackend as jwt_auth
from marshmallow import Schema, fields, validate, ValidationError

from database.models import User

class AuthSessionResource(object):
    def __init__(self, auth_backend):
        self.auth_backend = auth_backend

    def on_post(self, req, resp):
        session = req.context.session
        try:
            valid_args = AuthSessionValidator().validate(req.media, session)
            user = User.get_by_username(valid_args['username'], session)
            payload = {'username': user.username}
            token = self.auth_backend.get_auth_token(payload)
            response_data = {
                'token': token,
                'user': user.as_dict(req.prefix)
            }
            resp.status = falcon.HTTP_OK
            resp.body = json.dumps(response_data)
        except Exception as error:
            output = {
                'message': str(error)
            }
            resp.status = falcon.HTTP_500
            resp.body = json.dumps(output)


class AuthSessionValidator(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)

    def validate(self, raw_args, session):
        try:
            valid_args = self.load(raw_args)

            user = User.get_by_username(valid_args['username'], session)
            if not user or not check_password_hash(
                    user.password, valid_args['password']):
                raise ValidationError(
                    'Could not login',
                    'Username or password invalid.'
                )

            return valid_args
        except ValidationError as err:
            raise falcon.HTTPBadRequest(
                "Validation error",
                err.messages)
