import falcon
import json
import uuid

from marshmallow import Schema, fields, validate, ValidationError

from database.models import Product


class ProductsResource(object):
    def __init__(self, image_store):
        self.image_store = image_store

    def on_get(self, req, resp, public_id):
        product = Product.get(public_id, req.context.session)
        response_data = product.as_dict(req.prefix)
        output = response_data
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(output)

    def on_put(self, req, resp, public_id):
        session = req.context.session
        Validator = ValidatorBuilder.build('Put')
        args = Validator().validate(req._params)
        put_product = Product.update(
            public_id, args, self.image_store, session)
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(put_product.as_dict(req.prefix))

    def on_patch(self, req, resp, public_id):
        session = req.context.session
        Validator = ValidatorBuilder.build('Patch')
        args = Validator().validate(req._params)
        patched_product = Product.update(
            public_id, args, self.image_store, session)
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(patched_product.as_dict(req.prefix))

    def on_delete(self, req, resp, public_id):
        session = req.context.session
        deleted_public_id = Product.delete(
            public_id, session, self.image_store)
        resp.body = json.dumps({'public_id': deleted_public_id})

    def on_get_collection(self, req, resp):
        user = req.context['user']
        session = req.context.session
        products = Product.get_list(user, session)
        response_data = []
        for product in products:
            product_dict = product.as_dict(req.prefix)
            response_data.append(product_dict)
        output = response_data
        resp.body = json.dumps(output)

    def on_post_collection(self, req, resp):
        session = req.context.session
        user = req.context['user']
        raw_data = req._params
        Validator = ValidatorBuilder.build('Post')
        data = Validator().validate(raw_data)
        new_product = Product()
        new_product.save(user, data, self.image_store, session)
        output = new_product.as_dict(req.prefix)
        product_uri = req.url + '/' + new_product.public_id
        resp.location = product_uri
        resp.status = falcon.HTTP_201
        resp.body = json.dumps(output)

    def on_get_random(self, req, resp):
        session = req.context.session
        user = req.context['user']
        random_product = Product.get_random(user, session)
        response_data = random_product.as_dict(req.prefix)
        image_uri = req.prefix + '/products/' + random_product.public_id
        resp.location = image_uri
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(response_data)


class ValidatorBuilder(Schema):
    def validate(self, raw_args):
        try:
            valid_params = self.load(raw_args)
            return valid_params
        except ValidationError as err:
            raise falcon.HTTPBadRequest(
                "Validation error",
                err.messages)

    @classmethod
    def build(cls, field_type):
        fields_requirements = {
            'Post': {
                'title': fields.Str(required=True),
                'description': fields.Str(required=False),
                'link': fields.Str(required=False),
                'image': fields.Field(requied=False)
            },
            'Put': {
                'title': fields.Str(required=True),
                'description': fields.Str(required=True),
                'link': fields.Str(required=True),
                'image': fields.Field(required=True),
                'acquired': fields.Boolean(required=True)
            },
            'Patch': {
                'title': fields.Str(required=False),
                'description': fields.Str(required=False),
                'link': fields.Str(required=False),
                'image': fields.Field(required=False),
                'acquired': fields.Boolean(required=False)
            }
        }
        return type(f'{field_type}Schema', (cls,),
                    fields_requirements[field_type])
