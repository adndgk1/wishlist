import io
import os
import re
import uuid
import mimetypes
import json

import falcon
import msgpack

class ImagesResource(object):

    def __init__(self, auth_backend, image_store):
        self._image_store = image_store
        self.auth_backend = auth_backend
        self.auth = {
           'backend': auth_backend,
            'exempt_methods': ['GET']
        }

    def on_get(self, req, resp, name):
        resp.content_type = mimetypes.guess_type(name)[0]
        resp.stream, resp.content_length = self._image_store.open(name)