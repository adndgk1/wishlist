import falcon
from falcon_auth import JWTAuthBackend
import json
from marshmallow import Schema, fields, validate, ValidationError
from sqlalchemy import exc
from werkzeug.security import generate_password_hash, check_password_hash

from database.models import User


class UsersResource(object):

    def __init__(self, auth_backend):
        self.auth_backend = auth_backend
        self.auth = {
            'backend': auth_backend,
            'exempt_methods': ['POST']
        }

    def on_get(self, req, resp, public_id):
        session = req.context.session

        user = req.context['user']
        if not user.is_admin:
            raise falcon.HTTPUnauthorized(
                'User unauthorized',
                'User does not have access privilegies')

        user = User.get(public_id, session)
        response_data = user.as_dict(req.prefix)
        output = response_data

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(output)

    def on_put(self, req, resp, public_id):
        session = req.context.session

        user = req.context['user']
        if not user.is_admin:
            raise falcon.HTTPUnauthorized(
                'User unauthorized',
                'User does not have access privilegies')

        updated_user = User.put(req.media, public_id, session)

        resp.status = falcon.HTTP_OK
        resp.body = json.dumps(updated_user.as_dict(req.prefix))

    def on_delete(self, req, resp, public_id):
        session = req.context.session
        user = req.context['user']
        if not user.is_admin:
            raise falcon.HTTPUnauthorized(
                'User unauthorized',
                'User does not have access privilegies')
        deleted_public_id = User.delete(public_id, session)
        resp.body = json.dumps({'public_id': deleted_public_id})

    def on_get_collection(self, req, resp):
        session = req.context.session
        user = req.context['user']
        if not user.is_admin:
            raise falcon.HTTPUnauthorized(
                'User unauthorized',
                'User does not have access privilegies')

        users = User.get_list(session)
        response_data = []
        for user in users:
            response_data.append(user.as_dict(req.prefix))

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(response_data)

    def on_post_collection(self, req, resp):
        session = req.context.session
        args = AccountValidator().validate(req.media, session)

        new_user = User(username=args['username'], password=args['password'])
        new_user.save(req.context.session)

        payload = {'username': new_user.username}
        token = self.auth_backend.get_auth_token(payload)
        response_data = {
            'token': token,
            'user': new_user.as_dict(req.prefix)
        }
        resp.status = falcon.HTTP_201
        resp.body = json.dumps(response_data)


class AccountValidator(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    confirm_password = fields.Str(required=True)

    def validate(self, raw_args, session):
        try:
            valid_args = self.load(raw_args)
            if valid_args['password'] != valid_args['confirm_password']:
                raise ValidationError(
                    'Invalid passwords values',
                    'Password values must be equal')

            if User.exists(valid_args['username'], session):
                raise ValidationError(
                    'Username exists',
                    'Could not create user due to username already existing'
                )
            return valid_args
        except ValidationError as err:
            raise falcon.HTTPBadRequest(
                "Validation error",
                err.messages)
