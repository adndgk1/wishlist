import sys

from database.manager import DBManager

def main(db):
    command_map = {
        'createtables': create_tables,
        'droptables': drop_tables,
        'recreatetables': recreate_tables
    }
    
    command = sys.argv[1]
    try:
        assert command in command_map
    except:
        help(command, command_map)
    else:
        command_map[command](db,)

def help(wrong_cmd, cmd_map):
    print("==============================================================")
    print(f"    Invalid command '{wrong_cmd}'.")
    print(f"    Available commands:")
    for command in cmd_map:
        print(f'        - {command}')
    print("==============================================================")

def create_tables(db):
    print(' - Creating tables...')
    db.create_all_tables()
    db.create_admin()

def drop_tables(db):
    print(' - Droping tables...')
    db.drop_all_tables()

def recreate_tables(db):
    drop_tables(db)
    create_tables(db)
    print(' - Creating admin...')
    db.create_admin()

if __name__ == '__main__':
    db = DBManager()
    main(db)