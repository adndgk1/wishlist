import os

config = {
    'pg_user': os.getenv('POSTGRES_USER'),
    'pg_host': os.getenv('POSTGRES_HOST'),
    'pg_database': os.getenv('POSTGRES_DATABASE'),
    'pg_password': os.getenv('POSTGRES_PASSWORD'),
    'pg_port': os.getenv('POSTGRES_PORT'),
    'jwt_key': os.getenv('JWT_KEY'),
    'adm_name': os.getenv('ADMIN_USERNAME'),
    'adm_password': os.getenv('ADMIN_PASSWORD')
}