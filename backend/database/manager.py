from os import sys, path

import sqlalchemy
from sqlalchemy_utils import database_exists, create_database, drop_database
from sqlalchemy.orm import sessionmaker, scoping

from envconf import config
from .models import Base, User, Product


class DBManager():
    def __init__(self):
        url_template = ('postgresql://{pg_user}:{pg_password}' +
                        '@{pg_host}:{pg_port}/{pg_database}')
        self.engine = sqlalchemy.create_engine(url_template.format(**config))
        self.Session = scoping.scoped_session(
            sessionmaker(
                bind=self.engine,
                autocommit=True
            )
        )

        # create database, tables and admin if not created
        self.create_database()
        self.create_all_tables()
        self.create_admin()

    @property
    def session(self):
        return self.Session()

    def get_sqla_engine(self):
        return self.engine

    def create_database(self, replace=False):
        if replace:
            self.drop_database()
            create_database(self.engine.url)
        else:
            if not database_exists(self.engine.url):
                create_database(self.engine.url)

    def drop_database(self):
        drop_database(self.engine.url)

    def create_all_tables(self):
        Base.metadata.create_all(self.engine)

    def drop_all_tables(self):
        Base.metadata.drop_all(self.engine)

    def set_initial_configuration(self):
        self.drop_all_tables()
        self.create_all_tables()
        self.create_admin()

    def create_admin(self):
        User.create_admin(self.session)

    def get_admin(self):
        return User.get_admin(self.session)
