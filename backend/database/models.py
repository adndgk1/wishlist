import random
import falcon
import uuid
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash

from envconf import config

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    children = relationship("Product")
    public_fields = ['username', 'public_id', 'is_admin']

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False, index=True, unique=True)
    public_id = Column(String)
    password = Column(String)
    is_admin = Column(Boolean, default=False)

    def __init__(self, username, password):
        public_id = str(uuid.uuid4())
        password = generate_password_hash(password, method='sha256')
        super().__init__(username=username,
                         password=password,
                         public_id=public_id)

    def save(self, session):
        session.add(self)
        session.commit()

    def as_dict(self, url_prefix):
        user_dict = {}
        for col in self.__table__.columns:
            if col.name in self.public_fields:
                user_dict[col.name] = getattr(self, col.name)
        user_dict['uri'] = url_prefix + '/users/' + self.public_id
        return user_dict

    @classmethod
    def get_list(cls, session):
        return session.query(cls)

    @classmethod
    def get(cls, public_id, session):
        query = session.query(cls)
        result = query.filter_by(public_id=public_id).first()
        return result

    @classmethod
    def get_by_username(cls, username, session):
        query = session.query(cls)
        result = query.filter_by(username=username).first()
        return result

    @classmethod
    def exists(cls, username, session):
        query = session.query(cls)
        return query.filter_by(username=username).count() == 1

    @classmethod
    def put(cls, args, public_id, session):
        query = session.query(cls)
        updated_user = query.filter_by(public_id=public_id).first()
        for key, value in args.items():
            if key == 'password':
                updated_user.password = generate_password_hash(
                    args['password'], method='sha256')
            elif key == 'is_admin':
                updated_user.is_admin = args['is_admin']
        session.commit()
        return updated_user

    @classmethod
    def delete(cls, id, session):
        deleted_obj = cls.get(id, session)
        deleted_public_id = deleted_obj.public_id 
        query = session.query(cls)
        query.filter_by(public_id=deleted_public_id).delete()
        return deleted_public_id

    @classmethod
    def get_user_loader(cls, session):
        def user_loader(payload):
            username = payload['user']['username']
            user = session.query(cls).filter_by(username=username).one()
            session.close()
            return user
        return user_loader

    @classmethod
    def create_admin(cls, session):
        with session.begin():
            if session.query(cls).filter_by(is_admin=True).count() == 0:
                new_admin = cls(config['adm_name'], config['adm_password'])
                new_admin.is_admin = True
                session.add(new_admin)

    @classmethod
    def get_admin(cls, session):
        with session.begin():
            return session.query(cls).filter_by(username='admin').first()


class Product(Base):
    __tablename__ = 'products'

    public_fields = [
        'public_id',
        'title',
        'description',
        'link',
        'acquired',
        'image_uri']

    id = Column(Integer, primary_key=True)
    public_id = Column(String, default='')
    title = Column(String, default='')
    description = Column(String, default='')
    link = Column(String, default='')
    image_uri = Column(String, default='')
    acquired = Column(Boolean, default=False)
    username = Column(String, ForeignKey('users.username'), index=True)

    def save(self, user, data, image_store, session):
        try:
            for key, value in data.items():
                if key == 'image':
                    image_uri = image_store.save(data['image'])
                    setattr(self, 'image_uri', image_uri)
                setattr(self, key, value)

            self.public_id = str(uuid.uuid4())
            self.username = user.username
            session.add(self)

        except Exception:
            raise falcon.HTTPInternalServerError('Product could not be stored')
        else:
            session.commit()

    def as_dict(self, url_prefix):
        product_dict = {}
        for col in self.__table__.columns:
            if col.name in self.public_fields:
                if col.name == 'image_uri' and getattr(self, col.name) != '':
                    product_dict[col.name] = url_prefix + \
                        getattr(self, col.name)
                else:
                    product_dict[col.name] = getattr(self, col.name)
        product_dict['uri'] = url_prefix + '/products/' + self.public_id
        return product_dict

    @classmethod
    def get_list(cls, user, session):
        query = session.query(Product).filter_by(
            username=user.username).order_by(
            Product.id)
        return query.all()

    @classmethod
    def get(cls, public_id, session):
        query = session.query(Product)
        return query.filter_by(public_id=public_id).first()

    @classmethod
    def get_random(cls, user, session):
        query = session.query(cls).filter_by(username=user.username)
        return query.order_by(func.random()).first()

    @classmethod
    def update(cls, public_id, data, image_store, session):
        update_product = session.query(Product).filter_by(
            public_id=public_id).first()
        for key, value in data.items():
            if key == 'image':
                image_store.delete(update_product.image_uri)
                image_uri = image_store.save(data['image'])
                setattr(update_product, 'image_uri', image_uri)
            setattr(update_product, key, value)
        session.commit()
        return update_product

    @classmethod
    def delete(cls, public_id, session, image_store):
        deleted_obj = cls.get(public_id, session)
        deleted_public_id = deleted_obj.public_id
        image_store.delete(deleted_obj.image_uri)
        session.query(Product).filter_by(public_id=public_id).delete()
        return deleted_public_id
