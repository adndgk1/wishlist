import pytest
import falcon
from falcon_auth import JWTAuthBackend
from falcon import testing
import json
import copy
from collections import namedtuple
from requests_toolbelt.multipart.encoder import MultipartEncoder

import app
from database.manager import DBManager
from image_storage.image_store import ImageStore
from database.models import User
from envconf import config


class CommonFixture:
    @pytest.fixture()
    def db(self):
        return DBManager()

    @pytest.fixture(autouse=True)
    def db_setup(self, db):
        db.set_initial_configuration()
        return

    @pytest.fixture
    def image_store(self):
        storage_path = './image_storage/uploads'
        image_store = ImageStore(storage_path)
        image_store.remove_all_images()
        yield image_store
        image_store.remove_all_images()

    @pytest.fixture
    def client(self, image_store, autouse=True):
        api = app.create_api(image_store)
        return testing.TestClient(api)


class AuthFixture:
    @pytest.fixture
    def signup_params(self):
        signup_params = {
            'username': 'test_user',
            'password': 'test_password',
            'confirm_password': 'test_password'
        }
        return signup_params

    @pytest.fixture
    def signup_response(self, client, signup_params):
        response = client.simulate_post('/users', json=signup_params)
        return json.loads(response.content)

    @pytest.fixture
    def signup_user(self, signup_response):
        return signup_response['user']

    @pytest.fixture
    def login_params(self, client, signup_params):
        login_params = copy.deepcopy(signup_params)
        login_params.pop('confirm_password')
        return login_params

    @pytest.fixture
    def login_response(self, client, signup_response, login_params):
        response = client.simulate_post('/login', json=login_params)
        response_data = json.loads(response.content)
        return response_data

    @pytest.fixture
    def login_user(self, login_response):
        return login_response["user"]

    @pytest.fixture
    def login_token(self, login_response):
        return login_token

    @pytest.fixture
    def auth_headers(self, login_response):
        token = login_response['token']
        headers = {'Authorization': 'jwt ' + token}
        return headers

    @pytest.fixture
    def auth_backend(self, db):
        user_loader = User.get_user_loader(db.session)
        jwt_key = config['jwt_key']
        auth_backend = JWTAuthBackend(user_loader, jwt_key)
        return auth_backend

    @pytest.fixture
    def request_mocker(self):
        class RequestMocker(falcon.Request):
            def __init__(self):
                self.token = ''

            def get_header(self, name='Authorization'):
                return 'jwt ' + self.token
        return RequestMocker()

    @pytest.fixture
    def get_authenticated_user(self, db, request_mocker):
        user_loader = User.get_user_loader(db.session)
        jwt_key = config['jwt_key']
        auth_backend = JWTAuthBackend(user_loader, jwt_key)

        def _get_authenticated_user(token):
            request_mocker.token = token
            user = auth_backend.authenticate(request_mocker, None, None)
            return user
        return _get_authenticated_user


class ProductFixture:
    @pytest.fixture
    def posted_product(self, client, auth_headers, mpf_comp):
        post_components = mpf_comp('post', auth_headers)
        response = client.simulate_post('/products',
                                        headers=post_components.headers,
                                        body=post_components.body)
        return json.loads(response.content)

    @pytest.fixture
    def mpf_comp(self):
        def _mpf_comp(field_type, headers):
            MultipartFormComponents = namedtuple(
                'MultipartFormComponents', ["headers", "body"])

            fields = {
                'post': {
                    'title': 'Test Product',
                    'image': (
                        'test_image.jpg',
                        open('tests/resources/test_image2.jpg', 'rb'),
                        'image/jpeg')
                },
                'patch': {
                    'title': 'Patched Product',
                    'acquired': 'True',
                    'image': (
                        'test_image.jpg',
                        open('tests/resources/test_image2.jpg', 'rb'),
                        'image/jpeg')
                },
                'put': {
                    'title': 'Put Product',
                    'description': 'New description',
                    'link': 'http://newlink.com',
                            'acquired': 'True',
                            'image': ('', b'', '')
                }
            }
            mp_encoder = MultipartEncoder(fields=fields[field_type])
            headers = copy.deepcopy(headers)
            headers['Content-Type'] = mp_encoder.content_type
            headers['Content-Length'] = str(mp_encoder.len)
            body = mp_encoder.read()
            return MultipartFormComponents(headers=headers, body=body)
        return _mpf_comp


class AdminAuthFixture(AuthFixture):
    @pytest.fixture
    def login_params(self):
        login_params = {
            'username': config['adm_name'],
            'password': config['adm_password']
        }
        return login_params
