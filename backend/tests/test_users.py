import io
import falcon
import json
from werkzeug.security import check_password_hash

from tests.conftest import *
from database.manager import DBManager
from database.models import User


class TestUserResourceAsNotAuthenticatedUser(CommonFixture, AuthFixture):
    def test_on_post(self, client, signup_params, get_authenticated_user):
        response = client.simulate_post('/users', json=signup_params)
        assert response.status == falcon.HTTP_201
        response_data = json.loads(response.content)
        assert 'token' in response_data
        token = response_data['token']
        authenticated_user = get_authenticated_user(response_data['token'])
        assert authenticated_user.username == signup_params['username']

    def test_login(self, client, signup_user, login_params):
        response = client.simulate_post('/login', json=login_params)
        assert response.status == falcon.HTTP_200
        response_data = json.loads(response.content)
        login_user = response_data['user']
        assert login_user['public_id'] == signup_user['public_id']

    def test_on_get(self, signup_user, client):
        user_uri = f'/users/{ signup_user["public_id"] }'
        response = client.simulate_get(user_uri)
        assert response.status == falcon.HTTP_401

    def test_on_delete(self, signup_user, client):
        user_uri = f'/users/{ signup_user["public_id"] }'
        response = client.simulate_delete(user_uri)
        assert response.status == falcon.HTTP_401

    def test_on_put(self, signup_user, client):
        user_uri = f'/users/{ signup_user["public_id"] }'
        update_params = {'password': 'newpassword', 'is_admin': True}
        response = client.simulate_put(
            user_uri, json=update_params)
        assert response.status == falcon.HTTP_401

    def test_on_get_collection(self, client):
        response = client.simulate_get(f'/users')
        assert response.status == falcon.HTTP_401


class TestAsNonAdminAuthenticatedUser(CommonFixture, AuthFixture):
    def test_on_get(self, client, login_user, auth_headers):
        user_uri = f'/users/{ login_user["public_id"] }'
        response = client.simulate_get(user_uri, headers=auth_headers)
        assert response.status == falcon.HTTP_401

    def test_on_delete(self, client, login_user, auth_headers, db):
        public_id = login_user['public_id']
        user_uri = f'/users/{public_id}'
        response = client.simulate_delete(user_uri, headers=auth_headers)
        assert response.status == falcon.HTTP_401

    def test_on_put(self, client, auth_backend, db, auth_headers, login_user):
        user_uri = f'/users/{ login_user["public_id"] }'
        update_params = {'password': 'newpassword', 'is_admin': True}
        response = client.simulate_put(
            user_uri, json=update_params, headers=auth_headers)
        assert response.status == falcon.HTTP_401

    def test_on_get_collection(
        self,
        client,
        auth_headers,
        login_user
    ):
        response = client.simulate_get(f'/users', headers=auth_headers)
        assert response.status == falcon.HTTP_401


class TestUsersResourceAsAdmin(CommonFixture, AdminAuthFixture):

    def test_on_get(self, client, login_user, auth_headers):
        user_uri = f'/users/{ login_user["public_id"] }'
        response = client.simulate_get(user_uri, headers=auth_headers)
        retrieved_user = json.loads(response.content)
        print(retrieved_user)
        assert retrieved_user['public_id'] == login_user['public_id']

    def test_on_delete(self, client, login_user, auth_headers, db):
        public_id = login_user['public_id']
        user_uri = f'/users/{public_id}'
        assert User.get(public_id, db.session) is not None
        response = client.simulate_delete(user_uri, headers=auth_headers)
        deleted_user = json.loads(response.content)
        assert User.get(deleted_user['public_id'], db.session) is None

    def test_on_put(self, client, db, auth_headers, login_user):
        user_uri = f'/users/{ login_user["public_id"] }'
        update_params = {'password': 'newpassword', 'is_admin': True}
        response = client.simulate_put(
            user_uri, json=update_params, headers=auth_headers)
        updated_user = json.loads(response.content)
        assert updated_user['is_admin'] == update_params['is_admin']
        updated_user_db = User.get(updated_user['public_id'], db.session)
        assert check_password_hash(
            updated_user_db.password,
            update_params['password'])

    def test_on_get_collection(self, client, auth_headers, login_user):
        response = client.simulate_get(f'/users', headers=auth_headers)
        response_data = json.loads(response.content)
        # list returned should contain admin and test user
        assert len(response_data) == 2
