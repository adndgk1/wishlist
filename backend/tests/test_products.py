import io
import falcon
import json
import pytest

from tests.conftest import *

import app
from database.models import User
from image_storage.image_store import ImageStore
from database.models import Product


class TestProductResource(CommonFixture, AuthFixture, ProductFixture):

    def test_on_post(self, client, db, auth_headers, mpf_comp):
        post_components = mpf_comp('post', auth_headers)
        response = client.simulate_post('/products',
                                        headers=post_components.headers,
                                        body=post_components.body)
        created_product = json.loads(response.content)
        assert 'public_id' in created_product
        public_id = created_product['public_id']
        assert Product.get(public_id, db.session) is not None

    def test_on_get_collections(self, client, posted_product, auth_headers):
        response = client.simulate_get('/products', headers=auth_headers)
        assert response.status == falcon.HTTP_OK
        product_list = json.loads(response.content)
        assert isinstance(product_list, list)
        assert len(product_list) == 1
        product_from_list = product_list[0]
        assert product_from_list['public_id'] == posted_product['public_id']

    def test_on_get(self, client, posted_product, auth_headers):
        public_id = posted_product['public_id']
        response = client.simulate_get(f'/products/{public_id}',
                                       headers=auth_headers)
        assert response.status == falcon.HTTP_OK
        retrieved_product = json.loads(response.content)
        assert retrieved_product['public_id'] == posted_product['public_id']

    def test_on_delete(self, client, posted_product, auth_headers, db):
        public_id = posted_product['public_id']
        assert Product.get(public_id, db.session) is not None
        response = client.simulate_delete(f'/products/{public_id}',
                                          headers=auth_headers)
        assert response.status == falcon.HTTP_OK
        deleted_product = json.loads(response.content)
        assert deleted_product['public_id'] == posted_product['public_id']
        assert Product.get(public_id, db.session) is None

    def test_on_patch(self, client, posted_product, auth_headers, mpf_comp):
        # user in database does not own a product
        assert posted_product['acquired'] == False
        public_id = posted_product['public_id']
        patch_components = mpf_comp('patch', auth_headers)
        response = client.simulate_patch(f'/products/{ public_id }',
                                         headers=patch_components.headers,
                                         body=patch_components.body)
        assert response.status == falcon.HTTP_OK
        patched_product = json.loads(response.content)
        assert patched_product['acquired']
        assert patched_product["image_uri"] != posted_product["image_uri"]

    def test_put_product_item(
            self,
            client,
            posted_product,
            auth_headers,
            mpf_comp):
        public_id = posted_product['public_id']
        put_components = mpf_comp('put', auth_headers)
        response = client.simulate_put(f'/products/{ public_id }',
                                       headers=put_components.headers,
                                       body=put_components.body)
        assert response.status == falcon.HTTP_OK
        put_product = json.loads(response.content)

        # update should not change product public id
        assert put_product["public_id"] == posted_product["public_id"]

        # change all other properties
        assert put_product["title"] != posted_product["title"]
        assert put_product["description"] != posted_product["description"]
        assert put_product["link"] != posted_product["link"]
        assert put_product["acquired"] != posted_product["acquired"]
        assert put_product["image_uri"] != posted_product["image_uri"]
