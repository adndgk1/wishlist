import io

import wsgiref
from unittest.mock import call, MagicMock, mock_open

import falcon
from falcon import testing

import msgpack
import pytest

import app
from tests.conftest import *

class TestImageResource(CommonFixture, AuthFixture, ProductFixture):
    def test_on_get(self, client, posted_product):
        image_uri = posted_product['image_uri'] 
        image_name = image_uri.split('/')[-1]
        response = client.simulate_get('/images' + '/' + image_name)

        assert response.status == falcon.HTTP_OK